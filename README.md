# SIMD libraries on the Mandelbrot set (fractal shapes)

This project proposes to compare various SIMD libraries. The case study is the [Mandelbrot set](https://en.wikipedia.org/wiki/Mandelbrot_set).

## Compilation

Get the SIMD libraries from other git repositories:

    $ git submodule update --init --recursive

Compile the code on Linux/MacOS/MinGW:

    $ mkdir build
    $ cd build
    $ cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-Wall -funroll-loops -march=native"
    $ make -j4

On ARM 32-bit please add the `-mfpu=neon` option to enable the NEON instructions or `-mfpu=neon-vfpv4` to enable the NEON+FMA instructions (if available on your hardware).

## CMake options

The proposed `CMakeLists.txt` file can be adapted.

The `USE_FLOAT` option switches to single floating-point precision (by default this option is `OFF` and the implementations use double precision floating-point):

    $ cmake [...] -DUSE_FLOAT=ON

The `ENABLE_SDL` option enables the SDL output (by default this option is `ON` but if the SDL2 library can't be found on your system then the implementations will be compiled without SDL). To disable the SDL display, do as follow:

    $ cmake [...] -DENABLE_SDL=OFF

## Implementations

There are many SIMD implementations in this project. Notice that when the associated library can't be found, then the implementation compilation is skipped.

- Sequential: `fractal_seq` binary
- Intrinsics (SSE, AVX, AVX-512 and NEON): `fractal_intrinsics` binary 
- [AVX+FMA intrinsics](https://software.intel.com/sites/landingpage/IntrinsicsGuide/): `fractal_avx_fma` binary 
- [MIPP](https://github.com/aff3ct/MIPP): `fractal_mipp_lli` and `fractal_mipp_mli` binary 
- [Boost.simd](https://github.com/NumScale/boost.simd): `fractal_boost` binary 
- [OpenMP](http://hpac.rwth-aachen.de/teaching/pp-16/material/08.OpenMP-4.pdf): `fractal_omp` binary 
- [Vc](https://github.com/VcDevel/Vc): `fractal_vc` binary 
- [simdpp](https://github.com/p12tic/libsimdpp): `fractal_simdpp` binary
- [T-SIMD](http://www.ti.uni-bielefeld.de/html/people/moeller/tsimd_warpingsimd.html): `fractal_tsimd` binary
- [VCL](http://www.agner.org/optimize/vectorclass.pdf): `fractal_vcl` binary

Have fun :-)!
