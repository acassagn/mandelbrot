//https://indico.cern.ch/event/279723/sessions/52498/attachments/512260/706955/Vc_Workshop_ALICE_Offline_Week.pdf

#include <Vc/Vc>

#include "../fractal.h"

#ifdef USE_FLOAT
using FLOAT_v = Vc::float_v;
#else
using FLOAT_v = Vc::double_v;
#endif

constexpr size_t N = FLOAT_v::Size; /* number of elmts in a vector register */

static_assert(WIDTH % N == 0, "N has to be a multiple of WIDTH.");

std::string implem_name = "vc";
int vec_size = N;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
//	FLOAT_v two = 2.0;
	FLOAT_v bail = BAIL_OUT * BAIL_OUT;
#pragma omp parallel for
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x += N) {
			FLOAT_v m  = 0.0;
			FLOAT_v nv = 0.0;
			/* Get the complex poing on gauss space to be calculate */
			FLOAT init[N] __attribute__ ((aligned (64)));
			for (int i = 0; i < (int)N; i++) init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			FLOAT_v zr; zr.load(init);
			FLOAT_v cr = zr;
			FLOAT_v zi = centeri + (y - (HEIGHT/2)) / zoom;
			FLOAT_v ci = zi;
			/* Applies the actual mandelbrot formula on that point */
			for (int n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n++) {
				// implicit FMA
//				FLOAT_v a = zr*zr-zi*zi+cr;
////				FLOAT_v b = two * zr*zi + ci;
//				FLOAT_v b = (zr+zr)*zi+ci;
				// explicit FMA
				FLOAT_v a = Vc::fma(zr, zr, Vc::fma(-zi, zi, cr));
				FLOAT_v b = Vc::fma(zr, zi+zi, ci);
				zr = a;
				zi = b;
				m = a * a + b * b;
				auto mask = m < bail;
//				if (!Vc::any_of(mask))
				if (mask.isEmpty())
					break;
				nv(mask) += 1;
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number of iterations found */
			int color;
			FLOAT nn[N] __attribute__ ((aligned (64)));
			nv.store(nn);
			for (size_t i = 0; i < N; i++) {
				int n = (int)nn[i];
				if (n < maxiter)
					color = (MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color = 0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			nv.store(&ptr[y*WIDTH + x]);
#endif
		}
	}
}
