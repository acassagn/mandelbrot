#include <mipp.h>

#include "../fractal.h"

constexpr int N = mipp::N<FLOAT>(); /* number of elmts in a vector register */

static_assert(WIDTH % N == 0, "N has to be a multiple of WIDTH.");

std::string implem_name = "mipp_lli";
int vec_size = N;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
	mipp::reg one = mipp::set1<FLOAT>(1.0);
//	mipp::reg two = mipp::set1<FLOAT>(2.0);
	mipp::reg bail = mipp::set1<FLOAT>(BAIL_OUT * BAIL_OUT);
#pragma omp parallel for
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x += N) {
			mipp::reg m  = mipp::set0<FLOAT>();
			mipp::reg nv = mipp::set0<FLOAT>();
			/* Get the complex poing on gauss space to be calculate */
			FLOAT init[N] __attribute__ ((aligned (MIPP_REQUIRED_ALIGNMENT)));
			for (int i = 0; i < N; i++)
				init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			mipp::reg zr = mipp::load<FLOAT>(init);
//			mipp::reg zr = mipp::set<FLOAT>(init);
			mipp::reg cr = zr;
			mipp::reg zi = mipp::set1<FLOAT>(centeri + (y - (HEIGHT/2)) / zoom);
			mipp::reg ci = zi;
			/* Applies the actual mandelbrot formula on that point */
			for (int n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n++) {
				mipp::reg a = mipp::fmadd<FLOAT> (zr, zr, mipp::fnmadd<FLOAT>(zi, zi, cr));
//				mipp::reg b = mipp::fmadd<FLOAT> (two, mipp::mul<FLOAT>(zr,zi), ci);
				mipp::reg b = mipp::fmadd<FLOAT> (zr, mipp::add<FLOAT>(zi,zi), ci);
				zr = a;
				zi = b;
				m = mipp::fmadd<FLOAT>(a, a, mipp::mul<FLOAT>(b,b));
				mipp::msk mask = mipp::cmplt<FLOAT>(m, bail);
				if (mipp::testz<N>(mask))
					break;
//				nv = mipp::mask<FLOAT,mipp::add<FLOAT>>(mask, nv, nv, one);
				nv = mipp::add<FLOAT>(nv, mipp::andb<FLOAT>(mipp::toreg<N>(mask), one));
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number of iterations found */
			int color;
			FLOAT nn[N] __attribute__ ((aligned (MIPP_REQUIRED_ALIGNMENT)));
			mipp::store<FLOAT>(nn, nv);
			for (int i = 0; i < N; i++) {
				int n = (int)nn[i];
				if (n < maxiter)
					color = (MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color = 0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			mipp::store(&ptr[y*WIDTH + x], nv);
#endif
		}
	}
}
