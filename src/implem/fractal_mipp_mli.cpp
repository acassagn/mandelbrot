#include <mipp.h>

#include "../fractal.h"

constexpr int N = mipp::N<FLOAT>(); /* number of elmts in a vector register */

static_assert(WIDTH % N == 0, "N has to be a multiple of WIDTH.");

std::string implem_name = "mipp_mli";
int vec_size = N;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
//	mipp::Reg<FLOAT> two = 2.0;
	mipp::Reg<FLOAT> bail = BAIL_OUT * BAIL_OUT;
#pragma omp parallel for
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x += N) {
			mipp::Reg<FLOAT> m  = 0.0;
			mipp::Reg<FLOAT> nv = 0.0;
			/* Get the complex poing on gauss space to be calculate */
			FLOAT init[N] __attribute__ ((aligned (MIPP_REQUIRED_ALIGNMENT)));
			for (int i = 0; i < N; i++) init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			mipp::Reg<FLOAT> zr = init;
			mipp::Reg<FLOAT> cr = zr;
			mipp::Reg<FLOAT> zi = centeri + (y - (HEIGHT/2)) / zoom;
			mipp::Reg<FLOAT> ci = zi;
			/* Applies the actual mandelbrot formula on that point */
			for (int n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n++) {
				mipp::Reg<FLOAT> a = mipp::fmadd(zr, zr, mipp::fnmadd(zi, zi, cr));
//				mipp::Reg<FLOAT> b = mipp::fmadd(two, zr*zi, ci);
				mipp::Reg<FLOAT> b = mipp::fmadd(zr, zi+zi, ci);
				zr = a;
				zi = b;
				m = mipp::fmadd(a, a, b*b);
				mipp::Msk<N> mask = m < bail;
				if (mipp::testz(mask))
					break;
				nv += mipp::toReg<FLOAT>(mask) & 1;

			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number of iterations found */
			int color;
			FLOAT nn[N] __attribute__ ((aligned (MIPP_REQUIRED_ALIGNMENT)));
			nv.store(nn);
			for (int i = 0; i < N; i++) {
				int n = (int)nn[i];
				if (n < maxiter)
					color = (MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color = 0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			nv.store(&ptr[y*WIDTH + x]);
#endif
		}
	}
}
