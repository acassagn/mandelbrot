// https://developer.numscale.com/boost.simd/documentation/develop/tutorial-julia.html

#include <boost/simd.hpp>

#include <boost/simd/pack.hpp>
#include <boost/simd/function/aligned_store.hpp>
//#include <boost/simd/function/if_inc.hpp>
//#include <boost/simd/function/any.hpp>

#include "../fractal.h"

using pack_f = boost::simd::pack<FLOAT>;
constexpr size_t N = boost::simd::cardinal_of<pack_f>(); /* number of elmts in a vector register */

std::string implem_name = "boost.simd";
int vec_size = N;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
	pack_f bail{BAIL_OUT * BAIL_OUT};
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x += N) {
			pack_f m{0.0};
			pack_f nv{0.0};
			/* Get the complex poing on gauss space to be calculate */
			FLOAT init[N] __attribute__ ((aligned (64)));
			for (int i = 0; i < (int)N; i++) init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			pack_f zr(init, init + N);
			pack_f cr = zr;
			/* Get the complex poing on gauss space to be calculate */
			pack_f zi{centeri + (y - (HEIGHT/2)) / zoom};
			pack_f ci = zi;
			/* Applies the actual mandelbrot formula on that point */
			for (int n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n++) {
				pack_f a = zr * zr - zi * zi + cr;
				pack_f b = 2 * zr * zi + ci;
				zr = a;
				zi = b;
				m = a * a + b * b;
				auto mask = m < bail;
//				if (!boost::simd::any(mask)) // stop condition
//					break;
//				nv = boost::simd::if_inc(mask, nv); // increment if
				nv += pack_f(mask) & 1;
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number of iterations found */
			int color;
			FLOAT nn[N] __attribute__ ((aligned (64)));
			boost::simd::aligned_store(nv, &nn[0]);
			for (size_t i = 0; i < N; i++) {
				int n = (int)nn[i];
				if (n < maxiter)
					color = (MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color = 0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			boost::simd::aligned_store(nv, &ptr[y*WIDTH + x]);
#endif
		}
	}
}
