#include "../fractal.h"

#ifdef __AVX512F__
#include <immintrin.h>
#ifdef USE_FLOAT // -------------------------------------------------------------------------------------- AVX512 float
	#define START_MANDELBROT
	#define VECTOR_SIZE_16
	constexpr int VECTOR_SIZE = 16;
	using float_v  = __m512;
	using mfloat_v = __mmask16;
	static inline float_v  set1_v  (FLOAT s                               ) { return _mm512_set1_ps  (s);                       }
	static inline float_v  set_v   (FLOAT  s1, FLOAT  s2, FLOAT  s3, FLOAT  s4,
	                                FLOAT  s5, FLOAT  s6, FLOAT  s7, FLOAT  s8,
	                                FLOAT  s9, FLOAT s10, FLOAT s11, FLOAT s12,
	                                FLOAT s13, FLOAT s14, FLOAT s15, FLOAT s16) { return _mm512_setr_ps(s1, s2, s3, s4, s5, s6, s7, s8,
	                                                                                                    s9,s10,s11,s12,s13,s14,s15,s16); }
	static inline float_v  add_v   (float_v v1, float_v v2                ) { return _mm512_add_ps   (v1, v2);                  }
	static inline float_v  mul_v   (float_v v1, float_v v2                ) { return _mm512_mul_ps   (v1, v2);                  }
#ifdef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return _mm512_fmadd_ps (v1, v2, v3);              }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return _mm512_fnmadd_ps(v1, v2, v3);              }
#else
	static inline float_v  sub_v   (float_v v1, float_v v2                ) { return _mm512_sub_ps   (v1, v2);                  }
#endif
	static inline mfloat_v cmplt_v (float_v v1, float_v v2                ) { return _mm512_cmp_ps_mask(v1, v2, _CMP_LT_OS);    }
	static inline float_v  madd_v  (float_v src, mfloat_v m, float_v v1, float_v v2) { return _mm512_mask_add_ps(src, m, v1, v2); }
	static inline int      allz_v  (mfloat_v m                            ) { return _mm512_kortestz (m, m);                    }
	static inline float_v  load_v  (FLOAT* p                              ) { return _mm512_load_ps  (p);                       }
	static inline void     store_v (FLOAT* p, float_v v                   ) { return _mm512_store_ps (p, v);                    }
#else // ------------------------------------------------------------------------------------------------ AVX512 double
	#define START_MANDELBROT
	#define VECTOR_SIZE_8
	constexpr int VECTOR_SIZE = 8;
	using float_v  = __m512d;
	using mfloat_v = __mmask16;
	static inline float_v  set1_v  (FLOAT s                               ) { return _mm512_set1_pd  (s);                       }
	static inline float_v  set_v   (FLOAT s1, FLOAT s2, FLOAT s3, FLOAT s4,
	                                FLOAT s5, FLOAT s6, FLOAT s7, FLOAT s8) { return _mm512_setr_pd  (s1,s2,s3,s4,s5,s6,s7,s8); }
	static inline float_v  add_v   (float_v v1, float_v v2                ) { return _mm512_add_pd   (v1, v2);                  }
	static inline float_v  mul_v   (float_v v1, float_v v2                ) { return _mm512_mul_pd   (v1, v2);                  }
#ifdef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return _mm512_fmadd_pd (v1, v2, v3);              }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return _mm512_fnmadd_pd(v1, v2, v3);              }
#else
	static inline float_v  sub_v   (float_v v1, float_v v2                ) { return _mm512_sub_pd   (v1, v2);                  }
#endif
	static inline mfloat_v cmplt_v (float_v v1, float_v v2                ) { return _mm512_cmp_pd_mask(v1, v2, _CMP_LT_OS);    }
	static inline float_v  madd_v  (float_v src, mfloat_v m, float_v v1, float_v v2) { return _mm512_mask_add_pd(src, m, v1, v2); }
	static inline int      allz_v  (mfloat_v m                            ) { return _mm512_kortestz (m, m);                    }
	static inline float_v  load_v  (FLOAT* p                              ) { return _mm512_load_pd  (p);                       }
	static inline void     store_v (FLOAT* p, float_v v                   ) { return _mm512_store_pd (p, v);                    }
#endif
#ifndef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return add_v(mul_v(v1, v2), v3);                  }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return sub_v(v3, mul_v(v1, v2));                  }
#endif
#elif defined(__AVX__)
#include <immintrin.h>
#ifdef USE_FLOAT // ----------------------------------------------------------------------------------------- AVX float
	#define START_MANDELBROT
	#define VECTOR_SIZE_8
	constexpr int VECTOR_SIZE = 8;
	using float_v  = __m256;
	using mfloat_v = __m256;
	static inline float_v  set1_v  (FLOAT s                               ) { return _mm256_set1_ps  (s);                       }
	static inline float_v  set_v   (FLOAT s1, FLOAT s2, FLOAT s3, FLOAT s4,
	                                FLOAT s5, FLOAT s6, FLOAT s7, FLOAT s8) { return _mm256_setr_ps  (s1,s2,s3,s4,s5,s6,s7,s8); }
	static inline float_v  add_v   (float_v v1, float_v v2                ) { return _mm256_add_ps   (v1, v2);                  }
	static inline float_v  mul_v   (float_v v1, float_v v2                ) { return _mm256_mul_ps   (v1, v2);                  }
#ifdef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return _mm256_fmadd_ps (v1, v2, v3);              }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return _mm256_fnmadd_ps(v1, v2, v3);              }
#else
	static inline float_v  sub_v   (float_v v1, float_v v2                ) { return _mm256_sub_ps   (v1, v2);                  }
#endif
	static inline mfloat_v cmplt_v (float_v v1, float_v v2                ) { return _mm256_cmp_ps   (v1, v2, _CMP_LT_OS);      }
	static inline float_v  andb_v  (float_v v1, float_v v2                ) { return _mm256_and_ps   (v1, v2);                  }
	static inline int      allz_v  (mfloat_v m                            ) { return _mm256_testz_ps (m, set1_v(-1));           }
	static inline float_v  load_v  (FLOAT* p                              ) { return _mm256_load_ps  (p);                       }
	static inline void     store_v (FLOAT* p, float_v v                   ) { return _mm256_store_ps (p, v);                    }
#else // --------------------------------------------------------------------------------------------------- AVX double
	#define START_MANDELBROT
	#define VECTOR_SIZE_4
	constexpr int VECTOR_SIZE = 4;
	using float_v  = __m256d;
	using mfloat_v = __m256d;
	static inline float_v  set1_v  (FLOAT s                               ) { return _mm256_set1_pd  (s);                       }
	static inline float_v  set_v   (FLOAT s1, FLOAT s2, FLOAT s3, FLOAT s4) { return _mm256_setr_pd  (s1, s2, s3, s4);          }
	static inline float_v  add_v   (float_v v1, float_v v2                ) { return _mm256_add_pd   (v1, v2);                  }
	static inline float_v  mul_v   (float_v v1, float_v v2                ) { return _mm256_mul_pd   (v1, v2);                  }
#ifdef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return _mm256_fmadd_pd (v1, v2, v3);              }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return _mm256_fnmadd_pd(v1, v2, v3);              }
#else
	static inline float_v  sub_v   (float_v v1, float_v v2                ) { return _mm256_sub_pd   (v1, v2);                  }
#endif
	static inline mfloat_v cmplt_v (float_v v1, float_v v2                ) { return _mm256_cmp_pd   (v1, v2, _CMP_LT_OS);      }
	static inline float_v  andb_v  (float_v v1, float_v v2                ) { return _mm256_and_pd   (v1, v2);                  }
	static inline int      allz_v  (mfloat_v m                            ) { return _mm256_testz_pd (m, set1_v(-1));           }
	static inline float_v  load_v  (FLOAT* p                              ) { return _mm256_load_pd  (p);                       }
	static inline void     store_v (FLOAT* p, float_v v                   ) { return _mm256_store_pd (p, v);                    }
#endif
#ifndef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return add_v(mul_v(v1, v2), v3);                  }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return sub_v(v3, mul_v(v1, v2));                  }
#endif
#elif defined(__SSE4_1__)
#include <smmintrin.h>
#ifdef USE_FLOAT // ----------------------------------------------------------------------------------------- SSE float
	#define START_MANDELBROT
	#define VECTOR_SIZE_4
	constexpr int VECTOR_SIZE = 4;
	using float_v  = __m128;
	using mfloat_v = __m128;
	static inline float_v  set1_v  (FLOAT s                               ) { return _mm_set1_ps  (s);                          }
	static inline float_v  set_v   (FLOAT s1, FLOAT s2, FLOAT s3, FLOAT s4) { return _mm_setr_ps  (s1,s2,s3,s4);                }
	static inline float_v  add_v   (float_v v1, float_v v2                ) { return _mm_add_ps   (v1, v2);                     }
	static inline float_v  mul_v   (float_v v1, float_v v2                ) { return _mm_mul_ps   (v1, v2);                     }
#ifdef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return _mm_fmadd_ps (v1, v2, v3);                 }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return _mm_fnmadd_ps(v1, v2, v3);                 }
#else
	static inline float_v  sub_v   (float_v v1, float_v v2                ) { return _mm_sub_ps   (v1, v2);                     }
#endif
	static inline mfloat_v cmplt_v (float_v v1, float_v v2                ) { return _mm_cmplt_ps (v1, v2);                     }
	static inline float_v  andb_v  (float_v v1, float_v v2                ) { return _mm_and_ps   (v1, v2);                     }
	static inline int      allz_v  (mfloat_v m                            ) { return _mm_test_all_zeros(_mm_castps_si128(m), _mm_set1_epi32((int32_t)-1)); }
	static inline float_v  load_v  (FLOAT* p                              ) { return _mm_load_ps  (p);                          }
	static inline void     store_v (FLOAT* p, float_v v                   ) { return _mm_store_ps (p, v);                       }
#else // --------------------------------------------------------------------------------------------------- SSE double
	#define START_MANDELBROT
	#define VECTOR_SIZE_2
	constexpr int VECTOR_SIZE = 2;
	using float_v  = __m128d;
	using mfloat_v = __m128d;
	static inline float_v  set1_v  (FLOAT s                               ) { return _mm_set1_pd  (s);                          }
	static inline float_v  set_v   (FLOAT s1, FLOAT s2                    ) { return _mm_setr_pd  (s1, s2);                     }
	static inline float_v  add_v   (float_v v1, float_v v2                ) { return _mm_add_pd   (v1, v2);                     }
	static inline float_v  mul_v   (float_v v1, float_v v2                ) { return _mm_mul_pd   (v1, v2);                     }
#ifdef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return _mm_fmadd_pd (v1, v2, v3);                 }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return _mm_fnmadd_pd(v1, v2, v3);                 }
#else
	static inline float_v  sub_v   (float_v v1, float_v v2                ) { return _mm_sub_pd   (v1, v2);                     }
#endif
	static inline mfloat_v cmplt_v (float_v v1, float_v v2                ) { return _mm_cmplt_pd (v1, v2);                     }
	static inline float_v  andb_v  (float_v v1, float_v v2                ) { return _mm_and_pd   (v1, v2);                     }
	static inline int      allz_v  (mfloat_v m                            ) { return _mm_test_all_zeros(_mm_castpd_si128(m), _mm_set1_epi64x((int64_t)-1)); }
	static inline float_v  load_v  (FLOAT* p                              ) { return _mm_load_pd  (p);                          }
	static inline void     store_v (FLOAT* p, float_v v                   ) { return _mm_store_pd (p, v);                       }
#endif
#ifndef __FMA__
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return add_v(mul_v(v1, v2), v3);                  }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return sub_v(v3, mul_v(v1, v2));                  }
#endif
#elif defined(__ARM_NEON__) || defined(__ARM_NEON)
#include <arm_neon.h>
#ifdef USE_FLOAT // ---------------------------------------------------------------------------------------- NEON float
	#define START_MANDELBROT
	#define VECTOR_SIZE_4
	constexpr int VECTOR_SIZE = 4;
	using float_v  = float32x4_t;
	using mfloat_v = uint32x4_t;
	static inline float_v  set1_v  (FLOAT s                               ) { return vdupq_n_f32  (s);                          }
	static inline float_v  set_v   (FLOAT s1, FLOAT s2, FLOAT s3, FLOAT s4) { FLOAT a[VECTOR_SIZE] = {s1, s2, s3, s4}; return vld1q_f32(a); }
	static inline float_v  add_v   (float_v v1, float_v v2                ) { return vaddq_f32    (v1, v2);                     }
	static inline float_v  mul_v   (float_v v1, float_v v2                ) { return vmulq_f32    (v1, v2);                     }
#ifdef __ARM_FEATURE_FMA
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return vfmaq_f32    (v3, v1, v2);                 }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return vfmsq_f32    (v3, v1, v2);                 }
#else
	static inline float_v  sub_v   (float_v v1, float_v v2                ) { return vsubq_f32    (v1, v2);                     }
#endif
	static inline mfloat_v cmplt_v (float_v v1, float_v v2                ) { return vcltq_f32    (v1, v2);                     }
	static inline float_v  andb_v  (float_v v1, float_v v2                ) { return (float32x4_t)vandq_u32((uint32x4_t)v1, (uint32x4_t)v2); }
#ifdef __aarch64__
	static inline int      allz_v  (mfloat_v m                            ) { auto tmp = vorr_u64(vget_low_u64((uint64x2_t)m), vget_high_u64((uint64x2_t)m)); 
	                                                                          return !(int32_t)(vget_lane_u64(tmp, 0));         }
#else
	static inline int      allz_v  (mfloat_v m                            ) { uint32x2_t m2 = vorr_u32(vget_low_u32((uint32x4_t)m), vget_high_u32((uint32x4_t)m));
	                                                                          return !(vget_lane_u32(vpmax_u32(m2, m2), 0));    }
#endif
	static inline float_v  load_v  (FLOAT* p                              ) { return vld1q_f32    (p);                          }
	static inline void     store_v (FLOAT* p, float_v v                   ) { return vst1q_f32    (p, v);                       }
#elif defined(__aarch64__) // ----------------------------------------------------------------------------- NEON double
	#define START_MANDELBROT
	#define VECTOR_SIZE_2
	constexpr int VECTOR_SIZE = 2;
	using float_v  = float64x2_t;
	using mfloat_v = uint64x2_t;
	static inline float_v  set1_v  (FLOAT s                               ) { return vdupq_n_f64  (s);                          }
	static inline float_v  set_v   (FLOAT s1, FLOAT s2                    ) { FLOAT a[VECTOR_SIZE] = {s1, s2}; return vld1q_f64(a); }
	static inline float_v  add_v   (float_v v1, float_v v2                ) { return vaddq_f64    (v1, v2);                     }
	static inline float_v  mul_v   (float_v v1, float_v v2                ) { return vmulq_f64    (v1, v2);                     }
#ifdef __ARM_FEATURE_FMA
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return vfmaq_f64    (v3, v1, v2);                 }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return vfmsq_f64    (v3, v1, v2);                 }
#else
	static inline float_v  sub_v   (float_v v1, float_v v2                ) { return vsubq_f64    (v1, v2);                     }
#endif
	static inline mfloat_v cmplt_v (float_v v1, float_v v2                ) { return vcltq_f64    (v1, v2);                     }
	static inline float_v  andb_v  (float_v v1, float_v v2                ) { return (float64x2_t)vandq_u64((uint64x2_t)v1, (uint64x2_t)v2); }
	static inline int      allz_v  (mfloat_v m                            ) { auto tmp = vorr_u64(vget_low_u64(m), vget_high_u64(m)); 
	                                                                          return !(int32_t)(vget_lane_u64(tmp, 0));         }
	static inline float_v  load_v  (FLOAT* p                              ) { return vld1q_f64    (p);                          }
	static inline void     store_v (FLOAT* p, float_v v                   ) { return vst1q_f64    (p, v);                       }
#endif
#ifndef __ARM_FEATURE_FMA
	static inline float_v  fmadd_v (float_v v1, float_v v2, float_v v3    ) { return add_v(mul_v(v1, v2), v3);                  }
	static inline float_v  fnmadd_v(float_v v1, float_v v2, float_v v3    ) { return sub_v(v3, mul_v(v1, v2));                  }
#endif
#endif

#ifdef START_MANDELBROT

std::string implem_name = "intrinsics";
int vec_size = VECTOR_SIZE;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
	int x,y,n;
	float_v zr,zi,cr,ci;
//	float_v deux=set1_v(2);
	float_v un=set1_v(1);
//	float_v mun=set1_v(-1);
	float_v bail=set1_v(BAIL_OUT*BAIL_OUT);
#pragma omp parallel for private(x,zr,zi,cr,ci)
	for  (y = 0; y < HEIGHT; y++) {
		for (x = 0; x < WIDTH; x += VECTOR_SIZE) {
			float_v m = set1_v(0); //=0;
			float_v nv = set1_v(0);
			/* Get the complex poing on gauss space to be calculate */
//#ifdef __AVX512F__
			FLOAT init[VECTOR_SIZE] __attribute__ ((aligned (64)));
			for (int i = 0; i < VECTOR_SIZE; i++) init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			zr=cr=load_v(init);
//#else
//#ifdef VECTOR_SIZE_16
//			zr=cr=set_v(centerr + (x+ 0 - (WIDTH/2))/zoom,
//			            centerr + (x+ 1 - (WIDTH/2))/zoom,
//			            centerr + (x+ 2 - (WIDTH/2))/zoom,
//			            centerr + (x+ 3 - (WIDTH/2))/zoom,
//			            centerr + (x+ 4 - (WIDTH/2))/zoom,
//			            centerr + (x+ 5 - (WIDTH/2))/zoom,
//			            centerr + (x+ 6 - (WIDTH/2))/zoom,
//			            centerr + (x+ 7 - (WIDTH/2))/zoom,
//			            centerr + (x+ 8 - (WIDTH/2))/zoom,
//			            centerr + (x+ 9 - (WIDTH/2))/zoom,
//			            centerr + (x+10 - (WIDTH/2))/zoom,
//			            centerr + (x+11 - (WIDTH/2))/zoom,
//			            centerr + (x+12 - (WIDTH/2))/zoom,
//			            centerr + (x+13 - (WIDTH/2))/zoom,
//			            centerr + (x+14 - (WIDTH/2))/zoom,
//			            centerr + (x+15 - (WIDTH/2))/zoom);
//#elif defined(VECTOR_SIZE_8)
//			zr=cr=set_v(centerr + (x+ 0 - (WIDTH/2))/zoom,
//			            centerr + (x+ 1 - (WIDTH/2))/zoom,
//			            centerr + (x+ 2 - (WIDTH/2))/zoom,
//			            centerr + (x+ 3 - (WIDTH/2))/zoom,
//			            centerr + (x+ 4 - (WIDTH/2))/zoom,
//			            centerr + (x+ 5 - (WIDTH/2))/zoom,
//			            centerr + (x+ 6 - (WIDTH/2))/zoom,
//			            centerr + (x+ 7 - (WIDTH/2))/zoom);
//#elif defined(VECTOR_SIZE_4)
//			zr=cr=set_v(centerr + (x+ 0 - (WIDTH/2))/zoom,
//			            centerr + (x+ 1 - (WIDTH/2))/zoom,
//			            centerr + (x+ 2 - (WIDTH/2))/zoom,
//			            centerr + (x+ 3 - (WIDTH/2))/zoom);
//#elif defined(VECTOR_SIZE_2)
//			zr=cr=set_v(centerr + (x+ 0 - (WIDTH/2))/zoom,
//			            centerr + (x+ 1 - (WIDTH/2))/zoom);
//#else
//			std::cerr << "compute_mandelbrot: unsupported vector size :-(" << std::endl;
//			std::exit(-1);
//#endif
//#endif
			zi=ci=set1_v(centeri + (y - (HEIGHT/2))/zoom);
			/* Applies the actual mandelbrot formula on that point */
			for (n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n ++) {
				float_v a = fmadd_v(zr,zr,fnmadd_v(zi,zi,cr));
//				float_v b = fmadd_v(deux,mul_v(zr,zi),ci);
				float_v b = fmadd_v(zr,add_v(zi,zi),ci);
				zr = a;
				zi = b;
				m = fmadd_v(a,a,mul_v(b,b));
				mfloat_v mask = cmplt_v(m, bail);
				if (allz_v(mask)) break;
#ifdef __AVX512F__
				nv = madd_v(nv, mask, nv, un);
#else
				nv = add_v(nv, andb_v((float_v)mask,un));
#endif
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number
			of iterations found */
			int color;
			FLOAT nn[VECTOR_SIZE] __attribute__ ((aligned (64)));
			store_v(nn,nv);
			for (int i=0;i<VECTOR_SIZE; i++) {
				n=(int)nn[i];
				if (n<maxiter)
					color=(MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color=0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			store_v(&ptr[y*WIDTH + x], nv);
#endif
		}
	}
}
#else
#include <iostream>
#include <cstdlib>

std::string implem_name = "intrinsics";
int vec_size = 0;

void compute_mandelbrot(int maxiter, FLOAT centerr, FLOAT centeri, FLOAT zoom, uint32_t *pixels)
{
	std::cerr << "compute_mandelbrot: unsupported architecture :-(" << std::endl;
	int prec = 0;
#ifdef USE_FLOAT
	prec = 32;
#else
	prec = 64;
#endif
	std::cout << implem_name << "," << "NA" << "," << vec_size << "," << prec << "," << "NA" << std::endl;
	std::exit(0);
}
#endif
