#if defined(__ARM_NEON__) || defined(__ARM_NEON)
	#define SIMDPP_ARCH_ARM_NEON
	#define SIMDPP_ARCH_ARM_NEON_FLT_SP
#elif defined(__AVX512F__)
	#define SIMDPP_ARCH_X86_AVX512F
#elif defined(__AVX2__)
	#define SIMDPP_ARCH_X86_AVX2
#elif defined(__AVX__)
	#define SIMDPP_ARCH_X86_AVX
#elif defined(__SSE4_1__)
	#define SIMDPP_ARCH_X86_SSE4_1
#elif defined(__SSSE3__)
	#define SIMDPP_ARCH_X86_SSSE3
#elif defined(__SSE3__)
	#define SIMDPP_ARCH_X86_SSE3
#elif defined(__SSE2__)
	#define SIMDPP_ARCH_X86_SSE2
#elif defined(__SSE__)
	#define SIMDPP_ARCH_X86_SSE
#endif
#if defined(__FMA__)
	#define SIMDPP_ARCH_X86_FMA3
#endif

#include <simdpp/simd.h>

#include "../fractal.h"

#ifdef USE_FLOAT // try SIMDPP_FAST_FLOAT32_SIZE * 2, it's better...
constexpr int N = SIMDPP_FAST_FLOAT32_SIZE * 1; /* number of elmts in a vector register */
#else
constexpr int N = SIMDPP_FAST_FLOAT64_SIZE * 1; /* number of elmts in a vector register */
#endif

static_assert(WIDTH % N == 0, "N has to be a multiple of WIDTH.");

namespace simdpp {
#ifdef USE_FLOAT
using float_v = float32<N>;
using   int_v =   int32<N>;
using   int_s =   int32_t;
#else
using float_v = float64<N>;
using   int_v =   int64<N>;
using   int_s =   int64_t;
#endif
}

std::string implem_name = "simdpp";
int vec_size = N;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
	simdpp::int_v one = simdpp::make_int<simdpp::int_v>(1);
	simdpp::int_v zero = simdpp::make_int<simdpp::int_v>(0);
//	simdpp::float_v two = simdpp::make_float<simdpp::float_v>(2.0);
	simdpp::float_v bail = simdpp::make_float<simdpp::float_v>(BAIL_OUT * BAIL_OUT);
#pragma omp parallel for
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x += N) {
			simdpp::float_v m  = simdpp::make_float<simdpp::float_v>(0.0);
			simdpp::int_v nv = simdpp::make_int<simdpp::int_v>(0);
			/* Get the complex poing on gauss space to be calculate */
			FLOAT init[N] __attribute__ ((aligned (64)));
			for (int i = 0; i < N; i++) init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			simdpp::float_v zr = simdpp::load(init);
			simdpp::float_v cr = zr;
			simdpp::float_v zi = simdpp::make_float<simdpp::float_v>(centeri + (y - (HEIGHT/2)) / zoom);
			simdpp::float_v ci = zi;
			/* Applies the actual mandelbrot formula on that point */
			for (int n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n++) {
#if defined(__FMA__) // simdpp do not emulate the fmadd instr.
				simdpp::float_v a = simdpp::fmadd(zr, zr, simdpp::fmadd(0-zi, zi, cr));
//				simdpp::float_v b = simdpp::fmadd(two, zr*zi, ci);
				simdpp::float_v b = simdpp::fmadd(zr, zi+zi, ci);
#else
				simdpp::float_v a = zr * zr - zi * zi + cr;
//				simdpp::float_v b = simdpp::fmadd(two, zr*zi, ci);
				simdpp::float_v b = zr * (zi + zi) + ci;
#endif
				zr = a;
				zi = b;
#if defined(__FMA__) // simdpp do not emulate the fmadd instr.
				m = simdpp::fmadd(a, a, b*b);
#else
				m = a * a + b * b;
#endif
				auto mask = m < bail;
				if (!simdpp::test_bits_any(simdpp::blend(one, zero, mask)))
					break;
				nv = nv + (mask & one);
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number of iterations found */
			int color;
			simdpp::int_s nn[N] __attribute__ ((aligned (64)));
			simdpp::store(nn, nv);
			for (int i = 0; i < N; i++) {
				int n = (int)nn[i];
				if (n < maxiter)
					color = (MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color = 0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			simdpp::store(&ptr[y*WIDTH + x], nv);
#endif
		}
	}
}
