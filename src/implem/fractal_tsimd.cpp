#include <iostream>

#include <SIMDVecAll.H>

#if defined(__AVX__)
	constexpr int BW = 32;
#elif defined(__ARM_NEON__) || defined(__ARM_NEON) || defined(__SSE2__)
	constexpr int BW = 16;
#else
	constexpr int BW = 1;
	#define TSIMD_INCOMPATIBLE
#endif

#ifndef USE_FLOAT
#ifndef TSIMD_INCOMPATIBLE
#define TSIMD_INCOMPATIBLE
#endif
#endif

#include "../fractal.h"

constexpr int N = BW / sizeof(FLOAT); /* number of elmts in a vector register */

static_assert(WIDTH % N == 0, "N has to be a multiple of WIDTH.");

std::string implem_name = "tsimd";
int vec_size = N;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
#ifdef TSIMD_INCOMPATIBLE
	std::cerr << "compute_mandelbrot: unsupported architecture" << std::endl;
	int prec = 0;
#ifdef USE_FLOAT
	prec = 32;
#else
	prec = 64;
#endif
	std::cout << implem_name << "," << "NA" << "," << vec_size << "," << prec << "," << "NA" << std::endl;
	std::exit(0);
#else
	ns_simd::SIMDVec<FLOAT,BW> one = ns_simd::set1<FLOAT,BW>(1.0);
//	ns_simd::SIMDVec<FLOAT,BW> two = ns_simd::set1<FLOAT,BW>(2.0);
	ns_simd::SIMDVec<FLOAT,BW> bail = ns_simd::set1<FLOAT,BW>(BAIL_OUT * BAIL_OUT);
#pragma omp parallel for
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x += N) {
			ns_simd::SIMDVec<FLOAT,BW> m  = ns_simd::set1<FLOAT,BW>(0.0);
			ns_simd::SIMDVec<FLOAT,BW> nv = ns_simd::set1<FLOAT,BW>(0.0);
			/* Get the complex poing on gauss space to be calculate */
			FLOAT init[N] __attribute__ ((aligned (64)));
			for (int i = 0; i < N; i++) init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			ns_simd::SIMDVec<FLOAT,BW> zr = ns_simd::load<BW,FLOAT>(init);
			ns_simd::SIMDVec<FLOAT,BW> cr = zr;
			ns_simd::SIMDVec<FLOAT,BW> zi = ns_simd::set1<FLOAT,BW>(centeri + (y - (HEIGHT/2)) / zoom);
			ns_simd::SIMDVec<FLOAT,BW> ci = zi;
			/* Applies the actual mandelbrot formula on that point */
			for (int n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n++) {
				ns_simd::SIMDVec<FLOAT,BW> a = zr * zr - zi * zi + cr;
//				ns_simd::SIMDVec<FLOAT,BW> b = two * zr * zi + ci;
				ns_simd::SIMDVec<FLOAT,BW> b = zr * (zi + zi) + ci;
				zr = a;
				zi = b;
				m = a * a + b * b;
				ns_simd::SIMDVec<FLOAT,BW> mask = m < bail;
				if (ns_simd::test_all_zeros(mask))
					break;
				nv += mask & one;
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number of iterations found */
			int color;
			FLOAT nn[N] __attribute__ ((aligned (64)));
			ns_simd::store(nn, nv);
			for (int i = 0; i < N; i++) {
				int n = (int)nn[i];
				if (n < maxiter)
					color = (MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color = 0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			ns_simd::store(&ptr[y*WIDTH + x], nv);
#endif
		}
	}
#endif
}
