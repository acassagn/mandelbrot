#include "../fractal.h"

std::string implem_name = "avx_fma";
int vec_size = 4;

#if defined(__AVX__) && defined(__FMA__) && !defined(USE_FLOAT)
#include <immintrin.h>

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
	int x,y,n,i;
	__m256d zr,zi,cr,ci;
//	__m256d deux=_mm256_set1_pd(2);
	__m256d un=_mm256_set1_pd(1);
	__m256d mun=_mm256_set1_pd(-1);
	__m256d bail=_mm256_set1_pd(BAIL_OUT*BAIL_OUT);
#pragma omp parallel for private(x,zr,zi,cr,ci)
	for  (y = 0; y < HEIGHT; y++) {
		for (x = 0; x < WIDTH; x+=4) {
			__m256d m = _mm256_set1_pd(0); //=0;
			__m256d nv=_mm256_set1_pd(0);
			/* Get the complex poing on gauss space to be calculate */
			zr=cr=_mm256_set_pd(centerr + (x+3 - (WIDTH/2))/zoom,
			                    centerr + (x+2 - (WIDTH/2))/zoom,
			                    centerr + (x+1 - (WIDTH/2))/zoom,
			                    centerr + (x   - (WIDTH/2))/zoom);
			zi=ci=_mm256_set1_pd(centeri + (y - (HEIGHT/2))/zoom);
			/* Applies the actual mandelbrot formula on that point */
			for (n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n ++) {
				__m256d a=_mm256_fmadd_pd(zr,zr,_mm256_fnmadd_pd(zi,zi,cr));
//				__m256d b=_mm256_fmadd_pd(deux,_mm256_mul_pd(zr,zi),ci);
				__m256d b=_mm256_fmadd_pd(zr,_mm256_add_pd(zi,zi),ci);
				zr=a;
				zi=b;
				m=_mm256_fmadd_pd(a,a,_mm256_mul_pd(b,b));
				__m256d mask=_mm256_cmp_pd(m,bail,_CMP_LT_OS);
				if (_mm256_testz_pd(mask,mun)) break;
				nv=_mm256_add_pd(nv,_mm256_and_pd(mask,un));
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number
			of iterations found */
			int color;
			FLOAT nn[4] __attribute__ ((aligned (32)));;
			_mm256_store_pd(nn,nv);
			for (i=0;i<4; i++) {
				n=(int)nn[i];
				if (n<maxiter)
					color=(MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color=0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			_mm256_store_pd(&ptr[y*WIDTH + x], nv);
#endif
		}
	}
}
#else
#include <iostream>
#include <cstdlib>
void compute_mandelbrot(int maxiter, FLOAT centerr, FLOAT centeri, FLOAT zoom, uint32_t *pixels)
{
	std::cerr << "compute_mandelbrot: unsupported architecture (AVX+FMA on double is required)" << std::endl;
	int prec = 0;
#ifdef USE_FLOAT
	prec = 32;
#else
	prec = 64;
#endif
	std::cout << implem_name << "," << "NA" << "," << vec_size << "," << prec << "," << "NA" << std::endl;
	std::exit(0);
}
#endif
