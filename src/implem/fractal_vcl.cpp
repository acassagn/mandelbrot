#include <iostream>

#define VCL_NAMESPACE vcl
#ifdef __AVX512F__
#define MAX_VECTOR_SIZE 512 // enables AVX-512 in VCL
#endif
#include <vectorclass.h>

namespace vcl{
#if defined(__AVX512F__) || defined(__KNCI__) || defined(__MIC__)
	#ifdef USE_FLOAT
		constexpr int VECTOR_SIZE = 16;
		using Vecf  = Vec16f;
		using Vecfb = Vec16fb;
		using Veci  = Vec16i;
	#else
		constexpr int VECTOR_SIZE = 8;
		using Vecf  = Vec8d;
		using Vecfb = Vec8db;
		using Veci  = Vec8q;
	#endif
#elif defined(__AVX__)
	#ifdef USE_FLOAT
		constexpr int VECTOR_SIZE = 8;
		using Vecf  = Vec8f;
		using Vecfb = Vec8fb;
		using Veci  = Vec8i;
	#else
		constexpr int VECTOR_SIZE = 4;
		using Vecf  = Vec4d;
		using Vecfb = Vec4db;
		using Veci  = Vec4q;
	#endif
#elif defined(__SSE2__)
	#ifdef USE_FLOAT
		constexpr int VECTOR_SIZE = 4;
		using Vecf  = Vec4f;
		using Vecfb = Vec4fb;
		using Veci  = Vec8i;
	#else
		constexpr int VECTOR_SIZE = 2;
		using Vecf  = Vec2d;
		using Vecfb = Vec2db;
		using Veci  = Vec2q;
	#endif
#else
	#define VCL_INCOMPATIBLE
	constexpr int VECTOR_SIZE = 1;
#endif
}

#include "../fractal.h"

constexpr int N = vcl::VECTOR_SIZE; /* number of elmts in a vector register */

static_assert(WIDTH % N == 0, "N has to be a multiple of WIDTH.");

std::string implem_name = "vcl";
int vec_size = N;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
#ifdef VCL_INCOMPATIBLE
	std::cerr << "compute_mandelbrot: unsupported architecture" << std::endl;
	int prec = 0;
#ifdef USE_FLOAT
	prec = 32;
#else
	prec = 64;
#endif
	std::cout << implem_name << "," << "NA" << "," << vec_size << "," << prec << "," << "NA" << std::endl;
	std::exit(0);
#else
	vcl::Vecf one = 1.0;
//	vcl::Vecf two = 2.0;
	vcl::Vecf bail = BAIL_OUT * BAIL_OUT;
#pragma omp parallel for
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x += N) {
			vcl::Vecf m  = 0.0;
			vcl::Vecf nv = 0.0;
			/* Get the complex poing on gauss space to be calculate */
			FLOAT init[N] __attribute__ ((aligned (64)));
			for (int i = 0; i < N; i++) init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			vcl::Vecf zr; zr.load_a(init);
			vcl::Vecf cr = zr;
			vcl::Vecf zi = centeri + (y - (HEIGHT/2)) / zoom;
			vcl::Vecf ci = zi;
			/* Applies the actual mandelbrot formula on that point */
			for (int n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n++) {
				vcl::Vecf a = vcl::mul_add(zr, zr, vcl::nmul_add(zi, zi, cr));
//				vcl::Vecf b = vcl::mul_add(two, zr*zi, ci);
				vcl::Vecf b = vcl::mul_add(zr, zi+zi, ci);
				zr = a;
				zi = b;
				m = vcl::mul_add(a, a, b*b);
				vcl::Vecfb mask = m < bail;
				if (!vcl::horizontal_or(mask))
					break;
				nv = vcl::if_add(mask, nv, one);
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number of iterations found */
			int color;
			FLOAT nn[N] __attribute__ ((aligned (64)));
			nv.store_a(nn);
			for (int i = 0; i < N; i++) {
				int n = (int)nn[i];
				if (n < maxiter)
					color = (MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color = 0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			nv.store(&ptr[y*WIDTH + x]);
#endif
		}
	}
#endif
}
