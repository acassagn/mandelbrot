#include <xsimd/xsimd.hpp>
#include <mipp.h>

#include "../fractal.h"

constexpr int N = (int)xsimd::simd_traits<FLOAT>::size; /* number of elmts in a vector register */

static_assert(WIDTH % N == 0, "N has to be a multiple of WIDTH.");

std::string implem_name = "xsimd";
int vec_size = N;

void compute_mandelbrot(const int maxiter, const FLOAT centerr, const FLOAT centeri, const FLOAT zoom, uint32_t *pixels)
{
	xsimd::batch<FLOAT,N> one(1.0);
//	xsimd::batch<FLOAT,N> none(~one);
//	xsimd::batch<FLOAT,N> two(2.0);
	xsimd::batch<FLOAT,N> bail(BAIL_OUT * BAIL_OUT);
#pragma omp parallel for
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x += N) {
			xsimd::batch<FLOAT,N> m (0.0);
			xsimd::batch<FLOAT,N> nv(0.0);
			/* Get the complex poing on gauss space to be calculate */
			FLOAT init[N] __attribute__ ((aligned (64)));
			for (int i = 0; i < N; i++) init[i] = centerr + (x+i - (WIDTH/2))/zoom;
			xsimd::batch<FLOAT,N> zr = xsimd::load_aligned(init);
			xsimd::batch<FLOAT,N> cr = zr;
			xsimd::batch<FLOAT,N> zi(centeri + (y - (HEIGHT/2)) / zoom);
			xsimd::batch<FLOAT,N> ci = zi;
			/* Applies the actual mandelbrot formula on that point */
			for (int n = 0; n <= maxiter /*&& m < BAIL_OUT * BAIL_OUT*/; n++) {
				xsimd::batch<FLOAT,N> a = xsimd::fma(zr, zr, xsimd::fnma(zi, zi, cr));
//				xsimd::batch<FLOAT,N> b = xsimd::fma(two, zr*zi, ci);
				xsimd::batch<FLOAT,N> b = xsimd::fma(zr, zi+zi, ci);
				zr = a;
				zi = b;
				m = xsimd::fma(a, a, b*b);
				xsimd::batch_bool<FLOAT, N> mask = m < bail;
				if (!xsimd::any(mask))
					break;
//				nv += xsimd::bitwise_andnot(none, xsimd::batch<FLOAT,N>(mask));
				nv += xsimd::batch<FLOAT,N>(mask) & one;
			}
#ifdef USE_SDL
			/* Paint the pixel calculated depending on the number of iterations found */
			int color;
			FLOAT nn[N] __attribute__ ((aligned (64)));
			xsimd::store_aligned(nn, nv);
			for (int i = 0; i < N; i++) {
				int n = (int)nn[i];
				if (n < maxiter)
					color = (MAPPING[n%16][0]<<16)+(MAPPING[n%16][1]<<8)+(MAPPING[n%16][2]);
				else
					color = 0;
				pixels[y*WIDTH + x+i] = color;
			}
#else
			FLOAT* ptr = (FLOAT*)pixels;
			xsimd::store_aligned(&ptr[y*WIDTH + x], nv);
#endif
		}
	}
}
