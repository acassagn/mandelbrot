#!/usr/bin/env python3
import sys
import numpy as np
import pandas as pd

float_format='%.3g'

# fichier à lire, 'benchs.csv' par défaut
filename = 'benchs.csv'
if len(sys.argv) > 1:
    filename = sys.argv[1]

# lecture du CSV, ajout des headers
benchs = pd.read_csv(filename, names = ['timestamp','instr','implem','nbiters','vecsize','fpprec','time'])

# suppression des colonnes non utilisées
benchs = benchs.drop(['timestamp','nbiters'], axis=1)

# remplacement des '_' par des '.' dans la colonne 'implem'
benchs['implem'] = benchs['implem'].map(lambda s: s.replace('_','.'))

# -----

# extraction de tous les temps moyens par experimentation
groups = benchs.groupby(['instr','vecsize','fpprec','implem'])
aggregate = groups.mean()
aggregate.to_csv('avg_times.csv', na_rep='-', float_format=float_format)

# -----

# extraction des temps moyens pour chaque implem par experimentation
pivoted = benchs.pivot_table('time', index=['instr', 'vecsize','fpprec'], columns='implem')
groups = pivoted.groupby(['instr', 'vecsize','fpprec'])
aggregate = groups.mean()
aggregate.to_csv('implems.csv',na_rep='-', float_format=float_format)

# -----

# extraction: 1 fichier par jeu d'instruction, temps moyen par experimentation

# . liste des instructions
instr_levels = benchs['instr'].drop_duplicates()
for instr in instr_levels:
    # sous-ensemble des résultats pour cette instruction
    subset = benchs[benchs.instr == instr]

    # suppression de la colonne 'instr' sur le sous-ensemble (puisque l'instruction est unique sur le sous-ensemble)
    subset = subset.drop('instr', axis=1)

    # aggregation par moyennes sur le triplet (fpprec, vecsize, implem), fpprec en premier pour servir de référence pour le calcul de speedup
    groups = subset.groupby(['fpprec','vecsize','implem'])
    aggregate = groups.mean()

    # deep copy d'aggregate dans speedup car speedup est modifié par la suite, et on a besoin d'aggregate non modifié
    speedup = aggregate.copy(deep=True)

    # calcul de speedup pour chaque valeur de précision
    for prec in aggregate.index.get_level_values('fpprec').drop_duplicates():

        # valeur de référence pour le calcul de speedup (test sequentiel, vecteur de taille 1, pour la précision demandée)
        ref = aggregate.loc[prec,1,'seq']

        # modification du frame 'speedup' avec le résultat du calcul de speedup
        speedup.loc[(prec, slice(None), slice(None)), :] = ref / speedup.loc[(prec, slice(None), slice(None)), :]

    # renommage de la colonne 'time' en 'speedup' pour le frame 'speedup'
    speedup.rename(columns={'time':'speedup'}, inplace = True)

    # table pivotée: lignes indexées par implem, colonnes indexées par vecsize x fpprec (2 lignes d'entêtes)
    pivoted_subset = aggregate.pivot_table('time', index='implem', columns=['vecsize', 'fpprec'])

    # fusion et formattage des deux lignes d'entête pour avoir un seul entête par colonne
    pivoted_subset.columns = [''.join('%d x %d-bit' % header).strip() for header in pivoted_subset.columns.values]

    # export en CSV
    pivoted_subset.to_csv('instr_%s.csv' % instr,na_rep='-', float_format=float_format)

    # idem pour speedup: pivotage, renommage des entêtes, export CSV
    pivoted_subset = speedup.pivot_table('speedup', index='implem', columns=['vecsize', 'fpprec'])
    pivoted_subset.columns = [''.join('%d x %d-bit' % header).strip() for header in pivoted_subset.columns.values]
    pivoted_subset.to_csv('instr_speedup_%s.csv' % instr,na_rep='-', float_format=float_format)
