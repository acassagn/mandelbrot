#!/bin/bash

# number of iterations to compute
its=100
# instruction name
instr="neon"

if [ $# -gt 0 ]; then
	implems="$@"
else
	implems=../build/bin/fractal_*
fi

d=$(date -Iseconds)
for i in $implems; do
	if [ ! -x $i ]; then
		continue
	fi
	name=$(basename $i)
	if [ $name == ${name/#fractal} ]; then
		continue
	fi
	out=$(./$i $its 2> $name.log) || continue
	echo "\"$d\",$instr,$out" |tee -a benchs.csv
done
