set macro

set terminal pdf enhanced # font 'Helvetica,10' size 4,3
set encoding utf8

set datafile sep ','
set datafile missing '-'

set auto x
set style data histograms
set style histogram cluster gap 1
set style fill solid 0.7 border -1
set boxwidth 0.9
set xtics rotate by -45 scale 0

set ylabel 'Execution time (s)'

#set title 'Average execution time for each combination'
#set xlabel 'Vector size, element precision'
#set output 'processed_data.pdf'
#plot 'processed_data.csv' using 3: xticlabels(sprintf("(%d x %d-bit)",$1,$2)) ti col, \
#     for [i=4:13] ''      using i ti column(i)
#
#set title 'Average implementation execution time with 64-bit Floats'
#set xlabel 'Implementation'
#set output 'processed_data_fpprec64.pdf'
#plot 'processed_data_fpprec64.csv' using 2:xtic(1) ti col, \
#     for [i=3:4] ''                using i ti column(i)

set xlabel 'Implementation / Library'

set title 'Neon Instruction Set'
set output 'instr_neon.pdf'
plot 'instr_neon.csv' using 2:xtic(1) ti col, \
     for [i=3:7] ''  using i ti column(i)

set title 'SSE Instruction Set'
set output 'instr_sse.pdf'
plot 'instr_sse.csv' using 2:xtic(1) ti col, \
     for [i=3:7] ''  using i ti column(i)

set title 'AVX Instruction Set'
set output 'instr_avx.pdf'
plot 'instr_avx.csv' using 2:xtic(1) ti col, \
     for [i=3:7] ''  using i ti column(i)

set title 'AVX-512 Instruction Set'
set output 'instr_avx512.pdf'
plot 'instr_avx512.csv' using 2:xtic(1) ti col, \
     for [i=3:7] ''  using i ti column(i)

set ylabel 'Speedup'

set title 'Neon Instruction Set'
set output 'instr_speedup_neon.pdf'
plot 'instr_speedup_neon.csv' using 2:xtic(1) ti col, \
     for [i=3:7] ''  using i ti column(i)

set title 'SSE Instruction Set'
set output 'instr_speedup_sse.pdf'
plot 'instr_speedup_sse.csv' using 2:xtic(1) ti col, \
     for [i=3:7] ''  using i ti column(i)

set title 'AVX Instruction Set'
set output 'instr_speedup_avx.pdf'
plot 'instr_speedup_avx.csv' using 2:xtic(1) ti col, \
     for [i=3:7] ''  using i ti column(i)

set title 'AVX-512 Instruction Set'
set output 'instr_speedup_avx512.pdf'
plot 'instr_speedup_avx512.csv' using 2:xtic(1) ti col, \
     for [i=3:7] ''  using i ti column(i)

