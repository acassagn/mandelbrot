#!/bin/bash

set -x # debug mode

if [ $# -gt 0 ]; then
	install_loc="$@"
else
	install_loc="/opt/Vc"
fi

cd ..
git submodule update --init lib/Vc
cd lib/Vc
#git checkout 1.3

mkdir build
cd build

cmake -DCMAKE_INSTALL_PREFIX="$install_loc" -DBUILD_TESTING=OFF ..
make -j16
make install

cp "$install_loc"/lib/cmake/Vc/FindVc.cmake ../../../cmake/

escaped_install_loc=$(sed 's#/#\\/#g' <<< "$install_loc")
sed -i 's/\/opt\/Vc/'"$escaped_install_loc"'/g' ../../../cmake/FindVc.cmake
