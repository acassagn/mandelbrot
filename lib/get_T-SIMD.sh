#!/bin/bash

# mkdir T\-SIMD
# cd T\-SIMD
# wget http://www.ti.uni-bielefeld.de/html/people/moeller/SOFTWARE/WarpingSIMDStandAloneCode10_170410.tgz
# tar -xvvf WarpingSIMDStandAloneCode10_170410.tgz
# mv CODE10 include
# rm tar_code

mkdir T\-SIMD
cd T\-SIMD
wget http://www.ti.uni-bielefeld.de/html/people/moeller/SOFTWARE/WarpingSIMDStandAloneCode11_171207.tgz
tar -xvvf WarpingSIMDStandAloneCode11_171207.tgz
mv CODE11 include
rm tar_code

